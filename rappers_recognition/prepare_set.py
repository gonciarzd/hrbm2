import argparse
import os
from PIL import Image
from keras import utils
import numpy

def convertImage(img_size, path):
    img = Image.open(path)
    img = img.resize((img_size, img_size), Image.ANTIALIAS)
    imgAsArray = numpy.array(img, dtype=numpy.uint8)
    
    if imgAsArray.shape == (img_size, img_size):
        imgAsArray = numpy.repeat(imgAsArray[:, :, numpy.newaxis], 3, axis=2)

    if imgAsArray.shape == (img_size, img_size, 4):
        imgAsArray = imgAsArray[ ..., :3]

    if not imgAsArray.shape == (img_size, img_size, 3):
        print(imgAsArray.shape)

    return imgAsArray


def prepareSet(input_folder, img_size, test):
    samples = []
    labels = []
    classNumber = 0

    print('converting samples...')
    if test:
        #expects only one file at the time
        samples.append(convertImage(img_size, input_folder))
        samples = numpy.array(samples, dtype=numpy.uint8)
        return samples

    else :
        for dir in os.scandir(input_folder):
            if not dir.name.startswith('.') and dir.is_dir():
                print(dir.name)

                for file in os.scandir(input_folder + '/' + dir.name):
                    if not file.name.startswith('.') and file.is_file():
                        samples.append(convertImage(img_size, input_folder + '/' +  dir.name + '/' + file.name))
                        labels.append(classNumber)
                classNumber += 1
        samplesArray = numpy.array(samples, dtype=numpy.uint8)
        labels = utils.to_categorical(numpy.array(labels, dtype=numpy.uint8), 4)
        print(str(len(samplesArray)) + ' samples prepared')     
        return samplesArray, labels