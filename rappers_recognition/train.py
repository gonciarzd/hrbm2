#!/usr/bin/env python
import argparse
from keras.layers import Dense, Input
from keras import applications
from keras.models import Model, load_model
import numpy
import cv2
from .prepare_set import prepareSet
from keras.preprocessing.image import ImageDataGenerator

IMG_SIZE = 224
NO_CLASSES = 4

defaults = {
    'epochs': 70,
    'input_folder' : 'data',
    'model' : None,
    'test_file' : None
}

rappers = ['kękę', 'ostr', 'otsochodzi', 'taco_hemingway']

def predict(image, boxes):
    model = load_model('rappers_recognition/rappers_rec_model.h5')
    image_h, image_w, _ = image.shape
    faces = []
    if len(boxes) < 1: return []
    for box in boxes:
        xmin = int(box.xmin*image_w)
        ymin = int(box.ymin*image_h)
        xmax = int(box.xmax*image_w)
        ymax = int(box.ymax*image_h)

        face = image[ymin:ymax, xmin:xmax]
        face = cv2.resize(face, (IMG_SIZE, IMG_SIZE))
        face = numpy.array(face, dtype=numpy.uint8)
        if face.shape == (IMG_SIZE, IMG_SIZE):
            face = numpy.repeat(face[:, :, numpy.newaxis], 3, axis=2)

        if face.shape == (IMG_SIZE, IMG_SIZE, 4):
            face = face[ ..., :3]
        faces.append(face)

    faces = numpy.array(faces, dtype=numpy.uint8)
    predictions = model.predict(faces, verbose=1)
    labels = []
    
    for prediction in predictions:
        max_val = prediction.max()
        labels.append({ 'name': rappers[prediction.argmax()], 'accuracy': max_val.round(decimals=2, out=None) })
    return labels
    
    

def main(args):

    if args.model:
        print('loading model')
        model = load_model(args.model)
    else:
        print('generating new model')
        input_tensor = Input(shape=(IMG_SIZE, IMG_SIZE, 3))
        model = applications.resnet50.ResNet50(weights='imagenet', include_top=False, pooling='avg')

        output = model(input_tensor)
        output = Dense(4, activation='softmax')(output)
        model = Model(input_tensor, output)

        model.compile(
            optimizer='adam',
            loss='categorical_crossentropy',
            metrics=['accuracy', 'categorical_accuracy'])

    if args.test_file:
        test_image = prepareSet(args.test_file, IMG_SIZE, True)
        predictions = model.predict(test_image, batch_size=None, verbose=1, steps=None)
        print(predictions[0].round(decimals=2, out=None))
    else:
        datagen = ImageDataGenerator(validation_split=0.15)
        validation_generator = datagen.flow_from_directory(
            directory=args.input_folder,
            target_size=(IMG_SIZE, IMG_SIZE),
            batch_size=4,
            subset="validation"
        )
        train_generator = datagen.flow_from_directory(
            directory=args.input_folder,
            target_size=(IMG_SIZE, IMG_SIZE),
            batch_size=4,
            subset="training"
        )
        model.fit_generator(train_generator, steps_per_epoch=100, epochs=args.epochs, validation_data=validation_generator, validation_steps=700)
        model.save('rappers_rec_model.h5')

def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Trains CNN model on given dataset')
    parser.add_argument(
        '--epochs',
        help='Number of epochs to train.',
        type=int,
        default=defaults['epochs'])
    parser.add_argument(
        '--input-folder',
        help='Data input folder containing ',
        type=str,
        default=defaults['input_folder'])
    parser.add_argument(
        '--model',
        help='Path to pretrained model to use',
        type=str,
        default=defaults['model'])
    parser.add_argument(
        '--test-file',
        help='For given file and pretrained model returns classification',
        type=str,
        default=defaults['test_file'])
    return parser.parse_args()


if __name__ == '__main__':
    main(parse_arguments())
